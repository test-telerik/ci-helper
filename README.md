# ci-helper

## Purpose
The purpose of the `CI-HELPER` is simply to help us install HELM, Kubectl, install Gcloud-SDK and authenticate with gcloud

By this we will easly able to intiate the commands in the `gitlab-ci.yaml` in our `nginx-app`

## Workflow

Build --> Push to the docker registry in our custom container with appropriate tag.
