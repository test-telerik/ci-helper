FROM ubuntu

ARG HELM=v3.7.2

RUN apt-get update && apt-get install curl gnupg -y

#Kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

#Helm
ADD https://get.helm.sh/helm-${HELM}-linux-amd64.tar.gz /tmp
RUN tar xvf /tmp/helm-${HELM}-linux-amd64.tar.gz -C /tmp
RUN mv /tmp/linux-amd64/helm /usr/local/bin/helm

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
